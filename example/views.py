from django.conf import settings
from django.views.generic.simple import direct_to_template
from django_payworld.forms import PaymentForm
import django_payworld.config as config
from random import choice
from string import digits

import receivers


def payment(request):
    order_id = ''.join(choice(digits) for x in xrange(9))

    initial = {

            'order_id': order_id,
            'order_total': '10.00',
            'order_details': 'Test payment',
            'seller_name': settings.PAYWORLD_SELLER_NAME,
            'shop_id': settings.PAYWORLD_SHOP_ID,
    }
    form = PaymentForm(initial=initial)
    return direct_to_template(request, 'django_payworld/payment.html',
            {
                'payment_form': form,
                'url': config.PAYMENT_URL
            }
    )


